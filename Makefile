all: build

build:
	@go install .

test:
	@mv regular_cases.go regular_cases_test.go
	@go test
	@mv regular_cases_test.go regular_cases.go

test-parallel:
	@mv parallel_cases.go parallel_cases_test.go
	@go test
	@mv parallel_cases_test.go parallel_cases.go

run:
	@$$HOME/go/bin/program