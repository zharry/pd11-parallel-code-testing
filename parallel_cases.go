package main

import "testing"

func TestParallelTask(t *testing.T) {
	cases := GenerateTestCases()
	for _, c := range cases {
		t.Run("Task", func(t *testing.T) {
			t.Parallel()
			got := Task(c.in)
			if got != c.want {
				t.Errorf("Task(%q) == %q, want %q", c.in, got, c.want)
			}
		})
	}
}
