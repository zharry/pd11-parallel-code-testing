package main

import (
	"fmt"
	"time"
)

// Code from the tutorial
func main() {
	fmt.Println(ReverseRunes("Hello, World!"))
}
func ReverseRunes(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

// Task configuration
var taskTime time.Duration = 16

// Simulations of functions that take time to run
func Task(i int) int {
	time.Sleep(taskTime * time.Millisecond)
	return i
}

// Test Inputs
type taskIO struct{ in, want int }

var generateTime time.Duration = 0

// Simulation of non-parallel part
func GenerateTestCases() []taskIO {
	time.Sleep(generateTime * time.Millisecond)
	cases := []taskIO{
		{1, 1},
		{2, 2},
		{0, 0},
		{-1, -1},
	}
	return cases
}
