package main

import "testing"

func TestTask(t *testing.T) {
	cases := GenerateTestCases()
	for _, c := range cases {
		got := Task(c.in)
		if got != c.want {
			t.Errorf("Task(%q) == %q, want %q", c.in, got, c.want)
		}
	}
}
